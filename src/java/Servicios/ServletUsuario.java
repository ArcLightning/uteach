/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servicios;

import DAO.DaoAut;
import DAO.DaoUsuario;
import Modelo.Usuario;
import Util.Util;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Properties;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author Luisa
 */
public class ServletUsuario extends HttpServlet {

    DaoAut aut= new DaoAut();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

              

        PrintWriter out = response.getWriter();

        String correo = request.getParameter("correo");
        String contrasena = request.getParameter("contrasena");
       // String contrasena2 = request.getParameter("Contrasena2");
        String nombre = request.getParameter("nombre");
        String programa = request.getParameter("programa");
        String semestre = request.getParameter("semestre");
        String tipo = (request.getParameter("tipo"));
        boolean estado=false;

        try {

            Usuario usu = new Usuario();
            DaoUsuario u = new DaoUsuario();

            usu.setCorreo(correo);
            //if (contrasena.equals(contrasena2)) {
              usu.setContrasena(contrasena);  
            //}
            
            usu.setNombre(nombre);
            usu.setPrograma(programa);
            usu.setSemestre(Integer.parseInt(semestre));
            usu.setTipo(Integer.parseInt(tipo));
            usu.setId(u.idgenerado());
            usu.setEstado(estado);

            Util gson = new Util();
            String us;
            

            boolean resultado = u.insertarUsuario(usu);
            
            if (resultado = true) {
                us = gson.ToJson("POR FAVOR REALICE LA VERIFICACION DE SU CORREO ELECTRONICO");    
                   aut.correoVer(correo);
                   out.println(us); 
                 
            } else {
                us = gson.ToJson("INGRESE TODOS LOS DATOS PARA SU REGISTRO");
                out.println(us);
            }  
        } catch (SQLException ex) {
            Logger.getLogger(ServletUsuario.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }



    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            
            PrintWriter out = response.getWriter();
            Util gson=new Util();

            String correo = request.getParameter("correo");
            String contrasena = request.getParameter("contrasena");

            DaoUsuario u = new DaoUsuario();

            String respuesta = u.autenticacion(correo, contrasena);
            String usu = gson.ToJson(respuesta);
            out.println(usu);
            

            HttpSession session = request.getSession();
            if (correo != " " && correo != null) {

                session.setAttribute("usuarioGuardado", correo);
                

            }

        } catch (SQLException ex) {
            Logger.getLogger(ServletUsuario.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ServletUsuario.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(ServletUsuario.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ServletUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
