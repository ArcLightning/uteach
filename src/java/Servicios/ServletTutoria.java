/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servicios;

import DAO.DaoSesion;
import DAO.DaoTutoria;
import DAO.DaoUsuario;
import Modelo.Tutoria;
import Modelo.Usuario;
import Util.Util;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Luisa
 */
public class ServletTutoria extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
           
        }
    }

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
          try {
            
            HttpSession session = request.getSession();
            String ss = session.getAttribute("usuarioGuardado").toString();
            
            PrintWriter out = response.getWriter();
            Util gson= new Util();

            DaoTutoria t;
        
            t = new DaoTutoria();
            
            String tut = gson.ToJson(t.listarTutorias(ss));
            out.println(tut);
            //out.println(gson.ToJson(t.listarTutorias("luisaf.ariza")));
            
        } catch (SQLException ex) {
            Logger.getLogger(ServletTutoria.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ServletTutoria.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(ServletTutoria.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ServletTutoria.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try {
            
            HttpSession session = request.getSession();
            String ss = session.getAttribute("usuarioGuardado").toString();

            String materia = request.getParameter("materia");
            String dia = request.getParameter("dia");
            String hora = request.getParameter("hora");

           Date dd= new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            try {
                dd=formatter.parse(dia);
            } catch (ParseException ex) {
                Logger.getLogger(ServletTutoria.class.getName()).log(Level.SEVERE, null, ex);
            }
            DaoSesion d = new DaoSesion();
            Tutoria tu = new Tutoria();
            DaoUsuario id=new DaoUsuario();
            tu.setIdTutoria(id.idgenerado());
            
            Usuario u= d.ObjetoUsuario(ss);
            u.getId();
            u.getCorreo();
            tu.setTutor(u);
            tu.setMateria(materia);
            tu.setDia(dd);
            tu.setHora(hora);
            
            DaoTutoria t= new DaoTutoria();
            boolean rta=t.insertarTutoria(tu);
            
            
        } catch (SQLException ex) {
            Logger.getLogger(ServletTutoria.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ServletTutoria.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(ServletTutoria.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ServletTutoria.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
