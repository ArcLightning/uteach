
package Servicios;

import DAO.DaoCalificacion;
import DAO.DaoUsuario;
import Modelo.Calificacion;
import Util.Util;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class ServletCalificacion extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       

    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      
         HttpSession session = request.getSession();
         String ss = session.getAttribute("usuarioGuardado").toString();
         Util gson=new Util();
        
         String calificacion = request.getParameter("calificacion");
         
        try {
            Calificacion miC= new Calificacion();
            DaoCalificacion c = new DaoCalificacion();
            DaoUsuario du= new DaoUsuario();
            
            miC.setCalificacion(Integer.parseInt(calificacion));
            miC.setEstado(true);
            miC.setEstudiante(du.ObjetoUsuario(ss));
            miC.setIdCalificacion(0);
            miC.setTutor(du.ObjetoUsuario("67091"));
            
            Boolean respuesta=c.insertarCalificacion(miC);
            String cc = gson.ToJson(respuesta);
            out.println(cc);
            
            
        } catch (SQLException ex) {
            Logger.getLogger(ServletCalificacion.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ServletCalificacion.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(ServletCalificacion.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ServletCalificacion.class.getName()).log(Level.SEVERE, null, ex);
        }

            
            
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
