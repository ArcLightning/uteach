/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servicios;

import DAO.DaoSesion;
import DAO.DaoTutoria;
import Modelo.Sesion;
import Modelo.Usuario;
import Util.Util;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class ServletSesion extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
                       
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
             PrintWriter out = response.getWriter();
     
        try {
            
            Util gson= new Util();
            DaoTutoria u= new DaoTutoria();
            String usu= gson.ToJson(u.OfertaTutorias());
            out.println(usu);  
            
        } catch (SQLException ex) {
            Logger.getLogger(ServletSesion.class.getName()).log(Level.SEVERE, null, ex);     
    
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ServletSesion.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(ServletSesion.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ServletSesion.class.getName()).log(Level.SEVERE, null, ex);
        }   
        
    }

    int idSesion=51;
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        String ss = session.getAttribute("usuarioGuardado").toString();

        StringBuffer sbuf = new StringBuffer();

        String line = null;

        BufferedReader reader = request.getReader();
        while ((line = reader.readLine()) != null) {
            sbuf.append(line);

            Util u = new Util();
            u.ToJson(sbuf);
            System.out.println("si" + sbuf);

            JsonObject jsonObject = new JsonObject();
            jsonObject = (JsonObject) u.FromJson(sbuf.toString(), JsonObject.class);

            int id = jsonObject.get("id").getAsInt();
            String tema = jsonObject.get("tema").getAsString();
            String hora = jsonObject.get("hora").getAsString();
              
            try {
            DaoSesion s= new DaoSesion();
            
            Sesion sesion = new Sesion();
            
            
            sesion.setIdSesion(idSesion++);
            sesion.setEstudiante(s.ObjetoUsuario(ss));
            sesion.setTutor(s.ObjetoTutor(id));
            sesion.setTema(tema);
            sesion.setHora(hora);
            
            
            boolean resultado = s.insertarSesion(sesion);
            
            } catch (SQLException ex) {
                Logger.getLogger(ServletSesion.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ServletSesion.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(ServletSesion.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(ServletSesion.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

}
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
