<!DOCTYPE html>

  <html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Pagina Estudiante</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="dist/css/skins/skin-blue.min.css">
    <script src="js/Constantes.js" type="text/javascript"></script>
    <script src="js/jquery-3.2.0.js" type="text/javascript"></script>
    <script src="js/controllerTutorias.js" type="text/javascript"></script>
    <script src="js/controllerReservas.js" type="text/javascript"></script>
    

    </head>
  <body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <header class="main-header">

        <a class="logo">
          <span class="logo-mini"><b>U</b>T</span>
          <span class="logo-lg"><b>U</b>Teach</span>
        </a>

      <!-- Header Navbar -->
      <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">

            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="logout.jsp">
                <!-- The user image in the navbar-->
                <img src="dist/img/user.png" class="user-image" alt="User Image">
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span> Sing Out </span>
              </a>

          </ul>
        </div>
      </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="dist/img/user.png" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p>Nombre de Usuario</p>
            <!-- Status -->
            <a href="#"><i class="fa fa-circle text-success"></i>Estudiante</a>
          </div>
        </div>

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
          <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                  <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                  </button>
                </span>
          </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
          <li class="header">Opciones</li>
          <!-- Optionally, you can add icons to the links -->
          
          <li class="active"><a href="#" onclick="tutorias();"><i class="fa fa-link"></i> <span>Tutorias</span></a></li>
          
          <li class="treeview"><a onclick="reservas();" href="#"><i class="fa fa-link"></i> <span>Tutorias Reservadas</span></a></li>
          
        </ul>
        <!-- /.sidebar-menu -->
      </section>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Pagina Estudiante
          <small>Algo</small>
        </h1>
        
     
      </section>
        
      <!-- Main content -->
      <section class="content">

        <!-- Your Page Content Here -->
        
        <section class="content">
            
        <!-- TUTORIAS -->
            
        <div id="tutorias" style="display:none">

                    <div id="test3">
                            <!--Aqui van las tutorias   -->
                    </div>

                    <div class="box-footer"> 
                                <button onclick="verTutorias();" class="btn btn  btn-primary">Actualizar</a><br/>
                                <button onclick="cerrarTabla();" class="btn btn  btn-primary">Cerrar</button>
                    </div>
        </div>
            
        <!----------------------------- -->   
         
        <!-- MIS TUTORIAS -->
         
        <div id="reservas" style="display:none">
        <div class="col-md-12">
             <div class="box">
                 <div class="box-header with-border">
                     <h3 class="box-title">Tutorias Reservadas</h3>
                 </div>
                 <div class="box-body">
                     <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Nombre Tutor</th>
                                <th>Correo</th>
                                <th>Programa</th>
                                <th>Precio</th>
                                <th>Fecha</th>
                                <th>Hora</th>
                            </tr>
                        </thead>
                            <tbody id="test4">
                                <tr>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                </tr>
                            </tbody>
                    </table>
                    <div class="box-footer"> 
                            <button onclick="listaReservas();" class="btn btn  btn-primary ">Actualizar</a><br/>
                            <button onclick="cerrarTabla2();" class="btn btn  btn-primary ">Cerrar</button>
                    </div>
                 </div>
             </div>
           </div>
         </div>
        
        <!----------------------------- -->
        
        </section>                 
                               

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->

  </div>
  <!-- ./wrapper -->

  <!-- REQUIRED JS SCRIPTS -->

  <!-- jQuery 2.2.3 -->
  <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/app.min.js"></script>
  
<script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    function hideAll(){
        $("#tutorias").hide();
        $("#reservas").hide();
    }
    function tutorias(){
        hideAll();
        var cont = document.getElementById("tutorias");
        cont.style.display = "block";
        return true;
    }
    function cerrarTabla(){
        var cont = document.getElementById("tutorias");
        cont.style.display="none";
        return false;
    }
    function reservas(){
        hideAll();
        var cont = document.getElementById("reservas");
        cont.style.display = "block";
        return true;
    }
    function cerrarTabla2(){
        var cont = document.getElementById("reservas");
        cont.style.display="none";
        return false;
    }
    function mostrarFormu(){
            document.getElementById("modal").style.display = "block";
        }
    function ocultarFormu(){
        document.getElementById("modal").style.display = "none";
    }
    
    </script>


  </body>
  </html>
