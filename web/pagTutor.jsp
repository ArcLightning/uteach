  <!DOCTYPE html>

  <html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Pagina Tutor</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="dist/css/skins/skin-blue.min.css">
    
     <link rel="stylesheet" type="text/css" href="css/semantic.css"/>
     <link rel="stylesheet" type="text/css" href="css/tcal.css" />
    
    <script src="js/Constantes.js" type="text/javascript"></script>
    <script src="js/jquery-3.2.0.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/semantic_1.js"></script>
    <!--<script src="js/controller3Tutoria.js" type="text/javascript"></script>-->
   <script src="js/controllerCalificacion.js"type="text/javascript"></script> 
   <script src="js/controllerCalif.js"type="text/javascript"></script> 
    <script src="js/controllerTutCreadas.js" type="text/javascript"></script>
    
    <script src="js/controllerTutoria.js" type="text/javascript"></script>
    <script src="js/controllerSesCreadas.js" type="text/javascript"></script>
   
   
    
    <script type="text/javascript" src="js/tcal.js"></script> 
    
    </head>
  <body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <header class="main-header">

        <a class="logo">
          <span class="logo-mini"><b>U</b>T</span>
          <span class="logo-lg"><b>U</b>Teach</span>
        </a>

      <!-- Header Navbar -->
      <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">

            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="logout.jsp" class="" data-toggle="">
                <!-- The user image in the navbar-->
                <img src="dist/img/user.png" class="user-image" alt="User Image">
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span> Sing Out </span>
              </a>

          </ul>
        </div>
      </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="dist/img/user.png" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p>Nombre de Usuario</p>
            <!-- Status -->
            <a href="#"><i class="fa fa-circle text-success"></i>Tutor</a>
          </div>
        </div>

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
          <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                  <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                  </button>
                </span>
          </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
          <li class="header">Opciones</li>
          <!-- Optionally, you can add icons to the links -->
          
          <li class="active"><a href="#" onclick="formularioTutoria();"><i class="fa fa-link"></i> <span>Crear Tutoria</span></a></li>
          
          <li><a href="#" onclick="misTutorias();"><i class="fa fa-link"></i><span>Mis tutorias</span></a></li>
          
          <li class="treeview"><a onclick="solicitudes();" href="#"><i class="fa fa-link"></i> <span>Ya reservadas</span></a></li>
          
          <li class="treeview"><a href="#" onclick="calificacion()"><i class="fa fa-link"></i> <span>Mi Calificaion</span></a></li>
          
        </ul>
        <!-- /.sidebar-menu -->
      </section>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Pagina tutor
          <small>Algo</small>
        </h1>
        
     
      </section>
        
      <!-- Main content -->
      <section class="content">

        <!-- Your Page Content Here -->
        
        <section class="content">
            
        <!-- TUTORIAS -->
           <div id="formularioTutoria" style="display:none">
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Crear Tutoria</h3>
                        <form role="form">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Materia</label>
                                    <input id="materia" class="form-control">  
                                </div>
                                <div class="form-group">
                                    <label>Pecio $20.000</label>
                                </div>
                                <div class="form-group">
                                    <label>Dia</label>
                                    <form action="#">
		<div><input type="text" id="dia" name="date" class="tcal" value="" /></div>
	</form>
                                    
                                </div>
                                <div class="form-group">
                                    <label>Hora</label>
                                    <input id="hora" class="form-control">  
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" onclick="crearTutoria();" class="btn btn-primary">Crear</button>
                                <button type="submit" onclick="cerrarFormulario();" class="btn btn-primary">Cancelar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
          </div>
        <!----------------------------- -->   
         
        <!-- MIS TUTORIAS -->
         <div id="misTut" style="display:none">
         <div class="col-md-12">
             <div class="box">
                 <div class="box-header with-border">
                     <h3 class="box-title">Mis Tutorias</h3>
                 </div>
                 <div class="box-body">
                     <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Materia</th>
                                <th>Precio</th>
                                <th>Dia</th>
                                <th>Hora</th>
                            </tr>
                        </thead>
                            <tbody id="test">
                                <tr>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                </tr>
                            </tbody>
                    </table>
                    <div class="box-footer"> 
                            <button onclick="tutoriasCreadas();" class="btn btn  btn-primary ">Actualizar</a><br/>
                            <button onclick="cerrarTabla();" class="btn btn  btn-primary ">Cerrar</button>
                    </div>
                 </div>
             </div>
           </div>
         </div>
        <!----------------------------- -->
            
        <!-- TUTORIAS YA RESERVADAS -->
        <div id="solicitudes" style="display:none">
        <div class="col-md-12">
             <div class="box">
                 <div class="box-header with-border">
                     <h3 class="box-title">Mis Tutorias</h3>
                 </div>
                 <div class="box-body">
                     <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Tema</th>
                                <th>Precio</th>
                                <th>Hora</th>
                                <th>Fecha</th>
                                <th>Nombre</th>
                                <th>Correo</th>
                            </tr>
                        </thead>
                            <tbody id="test1">
                                <tr>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                </tr>
                            </tbody>
                    </table>
                    <div class="box-footer"> 
                            <button onclick="sesionesCreadas();" class="btn btn  btn-primary ">Actualizar</a><br/>
                            <button onclick="cerrarTabla2();" class="btn btn  btn-primary ">Cerrar</button>
                    </div>
                 </div>
             </div>
           </div>
         </div>
        <!----------------------------- -->
        
          <!-- TUTORIAS YA RESERVADAS -->
        <div id="calificacion" >
        <div class="col-md-12">
             <div class="box">
                 <div class="box-header with-border">
                     <h3 class="box-title">Calificacion</h3>
                 </div>
                 <div id="star" class="ui huge star rating"></div>
                 </div>
             </div>
           </div>
         </div>
        <!----------------------------- -->
        
        </section>                 
                               

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->

  </div>
  <!-- ./wrapper -->

  <!-- REQUIRED JS SCRIPTS -->

  <!-- jQuery 2.2.3 -->
  <!--<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>-->
  <!-- Bootstrap 3.3.6 -->
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/app.min.js"></script>
  <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    function hideAll(){
        $("#misTut").hide();
        $("#solicitudes").hide();
        $("#formularioTutoria").hide();
    }
    function misTutorias(){
        hideAll();
        var cont = document.getElementById("misTut");
        cont.style.display = "block";
        return true;
    }
    function solicitudes(){
        hideAll();
        var cont = document.getElementById("solicitudes");
        cont.style.display = "block";
        return true;
    }
    function calificacion(){
        hideAll();
        var cont = document.getElementById("calificacion");
        cont.style.display = "block";
        return true;
    }
    function formularioTutoria(){
        hideAll();
        var cont = document.getElementById("formularioTutoria");
        cont.style.display = "block";
        return true;
    }
    function cerrarFormulario(){
        var cont = document.getElementById("formularioTutoria");
        cont.style.display="none";
        return false;
    }
    function cerrarTabla(){
        var cont = document.getElementById("misTut");
        cont.style.display="none";
        return false;
    }
    function cerrarTabla2(){
        var cont = document.getElementById("solicitudes");
        cont.style.display="none";
        return false;
    }
    </script>

     

  </body>
  </html>
