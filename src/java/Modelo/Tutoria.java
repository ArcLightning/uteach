package Modelo;

import java.util.Date;

/**
 *
 * @author Nico
 */
public class Tutoria {
    
    private int idTutoria;
    private Usuario tutor;
    private String materia;
    private long precio;
    
    private String hora;
    
    Date dia= new Date();
    public Tutoria(){
        
    }

    public Date getDia() {
        return dia;
    }

    public void setDia(Date dia) {
        this.dia = dia;
    }

    public int getIdTutoria() {
        return idTutoria;
    }

    public void setIdTutoria(int idTutoria) {
        this.idTutoria = idTutoria;
    }

    public Usuario getTutor() {
        return tutor;
    }

    public void setTutor(Usuario tutor) {
        this.tutor = tutor;
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }

    public long getPrecio() {
        return precio;
    }

    public void setPrecio(long precio) {
        this.precio =15000;
    }

  
    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    @Override
    public String toString() {
        return "Tutoria{" + "idTutoria=" + idTutoria + ", tutor=" + tutor + ", materia=" + materia + ", precio=" + precio + ", dia=" + dia + ", hora=" + hora + '}';
    }

  
}
