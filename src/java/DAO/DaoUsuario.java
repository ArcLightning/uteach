package DAO;

import Modelo.Usuario;
import Util.Conexion;
import com.google.gson.GsonBuilder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;
import java.sql.ResultSet;
import java.util.ArrayList;
import static javax.ws.rs.core.Response.status;

/**
 *
 * @author Nico
 */
public class DaoUsuario {

    private Connection conexion;

    public DaoUsuario() throws SQLException {
        Conexion de = Conexion.getConexion();
        this.conexion = de.getConnection();

    }

    public boolean insertarUsuario(Usuario usuario) {
        boolean resultado = false;
        try {//!.Establecer la consulta
            String consulta = "INSERT INTO USUARIO VALUES(?,?,?,?,?,?,?,?)";
            //2.Crear el prepareStament
            PreparedStatement statement;
            statement = this.conexion.prepareStatement(consulta);
            //-------------------------------------------------
            statement.setInt(1, usuario.getId());
            statement.setString(2, usuario.getCorreo());
            statement.setString(3, usuario.getContrasena());
            statement.setString(4, usuario.getNombre());
            statement.setString(5, usuario.getPrograma());
            statement.setInt(6, usuario.getSemestre());
            statement.setInt(7, usuario.getTipo());
            statement.setBoolean(8, usuario.getEstado());
            //-----------------------------------------------

            //3.Hacer la ejecucion
            resultado = statement.execute();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return resultado;
    }

    public String autenticacion(String correo, String contraseña) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        String query;
        String bdcorreo, bdcontraseña;
        boolean bdestado;
        int bdtipo;
        String login = null;

        Class.forName("com.mysql.jdbc.Driver").newInstance();
        Connection con = DriverManager.getConnection(Util.Util.URL_DB, Util.Util.USER, Util.Util.PW);
        Statement stmt = (Statement) con.createStatement();
        query = "SELECT * FROM Usuario;";
        stmt.executeQuery(query);
        ResultSet rs = stmt.getResultSet();

        while (rs.next()) {

            bdcorreo = rs.getString("correo");
            bdcontraseña = rs.getString("contrasena");
            bdtipo = rs.getInt("tipo");
            bdestado = rs.getBoolean("estado");

            if ((bdcorreo.equals(correo) && bdcontraseña.equals(contraseña)) && (bdtipo == 1) && (bdestado = true)) {
                System.out.println("OK ESTUDIANTE");
                login = "Estudiante";
                break;
            } else if ((bdcorreo.equals(correo) && bdcontraseña.equals(contraseña)) && (bdtipo == 2) && (bdestado = true)) {
                System.out.println("OK TUTOR");
                login = "Tutor";
                break;

            }
        }
        return login;
    }

    public int idgenerado() {

        int max = 99999;
        int min = 00000;
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        System.out.println(randomNum);
        return randomNum;
    }

    public Usuario ObjetoUsuario(String s) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        String query;
        String bdcorreo, bdnombre, bdprograma;
        int bdsemestre;

        Usuario u = new Usuario();

        Class.forName("com.mysql.jdbc.Driver").newInstance();
        Connection con = DriverManager.getConnection(Util.Util.URL_DB, Util.Util.USER, Util.Util.PW);
        Statement stmt = (Statement) con.createStatement();
        query = "SELECT * FROM Usuario;";
        
        stmt.executeQuery(query);
        ResultSet rs = stmt.getResultSet();

        while (rs.next()) {

            bdcorreo = rs.getString("correo");
            bdnombre = rs.getString("nombre");
            bdprograma = rs.getString("programa");
            bdsemestre = rs.getInt("semestre");

            u.setCorreo(bdcorreo);
            u.setNombre(bdnombre);
            u.setPrograma(bdprograma);
            u.setSemestre(bdsemestre);

        }
        return u;

    }

    public void CambioEstadoCuenta(String correo) throws ClassNotFoundException,
            InstantiationException, IllegalAccessException, SQLException {
        String query;
        String bdcorreo;
        boolean bdestado;

        Class.forName("com.mysql.jdbc.Driver").newInstance();
        Connection con = DriverManager.getConnection(Util.Util.URL_DB, Util.Util.USER, Util.Util.PW);
        Statement stmt = (Statement) con.createStatement();
        query = "SELECT * FROM Usuario;";
        stmt.executeQuery(query);
        ResultSet rs = stmt.getResultSet();

        PreparedStatement statement;
        statement = this.conexion.prepareStatement(query);

        Usuario u= new Usuario();
        
        while (rs.next()) {

            bdcorreo = rs.getString("correo");
            bdestado = rs.getBoolean("estado");

            if (bdcorreo.equals(correo) && (bdestado = false)) {
                              
               u.setCorreo(rs.getString("correo"));
               u.setEstado(rs.getBoolean("true"));
               u.setContrasena(rs.getString("contrasena"));
               u.setId(rs.getInt("id"));
               u.setNombre(rs.getString("nombre"));
               u.setPrograma(rs.getString("programa"));
               u.setTipo(rs.getInt("tipo"));
               u.setSemestre(rs.getInt("semestre"));
                break;
            }

        }
        DaoUsuario da= new DaoUsuario();
        da.CambioEstadoCuenta(u.getCorreo());
      }
}
