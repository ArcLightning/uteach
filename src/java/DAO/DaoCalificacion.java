/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Modelo.Calificacion;
import Modelo.Usuario;
import Util.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Diseño
 */
public class DaoCalificacion {
    
     private Connection conexion;

    public DaoCalificacion() throws SQLException {
        Conexion de = Conexion.getConexion();
        this.conexion = de.getConnection();

    }

    public boolean insertarCalificacion(Calificacion cal) {
        boolean resultado = false;
        try {//!.Establecer la consulta
            String consulta = "INSERT INTO CALIFICACION VALUES(?,?,?,?,?)";
            //2.Crear el prepareStament
            PreparedStatement statement;
            statement = this.conexion.prepareStatement(consulta);
            //-------------------------------------------------
            statement.setInt(1, cal.getIdCalificacion());
            statement.setInt(2, cal.getTutor().getId());
            statement.setInt(3, cal.getEstudiante().getId());
            statement.setInt(4, cal.getCalificacion());
            statement.setBoolean(8, cal.isEstado());
            //-----------------------------------------------

            //3.Hacer la ejecucion
            resultado = statement.execute();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return resultado;
    }
    
    public int calcularPromedio(int cant){
        return 0;
       
    }
}
    

