/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servicios;

import DAO.DaoTutoria;
import Util.Util;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author 
 */
public class ServletTutor extends HttpServlet {

  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    
    }

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
         try {
            
            HttpSession session = request.getSession();
            String ss = session.getAttribute("usuarioGuardado").toString();
            
            PrintWriter out = response.getWriter();
            Util gson= new Util();

            DaoTutoria t;
        
            t = new DaoTutoria();
            
            String tut = gson.ToJson(t.listarSesionesSolicitadas(ss));
            out.println(tut);
            
        } catch (SQLException ex) {
            Logger.getLogger(ServletTutoria.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ServletTutoria.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(ServletTutoria.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ServletTutoria.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      
         try {
            
           HttpSession session = request.getSession();
           String ss = session.getAttribute("usuarioGuardado").toString();
            
            PrintWriter out = response.getWriter();
            Util gson= new Util();

            DaoTutoria t;
        
            t = new DaoTutoria();
            
            String tut = gson.ToJson(t.TutoriasCreadas(ss));
            out.println(tut);
            //out.println(gson.ToJson(t.listarTutorias("luisaf.ariza")));
            
        } catch (SQLException ex) {
            Logger.getLogger(ServletTutoria.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ServletTutoria.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(ServletTutoria.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ServletTutoria.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
