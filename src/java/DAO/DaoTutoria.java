/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Modelo.Sesion;
import Modelo.Tutoria;
import Modelo.Usuario;
import Util.Conexion;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Luisa
 */
public class DaoTutoria {
    
     private Connection conexion;
    public DaoTutoria() throws SQLException{
        Conexion de = Conexion.getConexion();
        this.conexion = de.getConnection();
        
    }
    
    public boolean insertarTutoria(Tutoria tutoria){
        boolean resultado = false;
        try {//!.Establecer la consulta
        String consulta="INSERT INTO TUTORIATUTOR VALUES(?,?,?,?,?,?)";
        //2.Crear el prepareStament
          PreparedStatement statement;
          statement=this.conexion.prepareStatement(consulta);
        //-------------------------------------------------
            statement.setInt(1,tutoria.getIdTutoria());
            statement.setInt(2,tutoria.getTutor().getId());
            statement.setString(3,tutoria.getMateria());
            statement.setLong(4,tutoria.getPrecio());
            statement.setDate(5, (Date) tutoria.getDia());
            statement.setString(6,tutoria.getHora());
          //-----------------------------------------------
          
        //3.Hacer la ejecucion
        resultado=statement.execute();
        
            
  }catch(SQLException ex){
        ex.printStackTrace();
    }
        return resultado;
}
     public  ArrayList<Usuario> listarTutores() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
             ArrayList<Usuario> u= new ArrayList<Usuario>();
             String query;
            String bdcorreo, bdcontraseña;
            int bdtipo;
            int login=0;
            int bdid;
            String bdnombre;
            String bdprograma;
            int bdsemestre;
            int tipo;
            
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection con=DriverManager.getConnection(Util.Util.URL_DB, Util.Util.USER, Util.Util.PW);
            Statement stmt=(Statement) con.createStatement();
            query="SELECT * FROM Usuario;";
            stmt.executeQuery(query);
            ResultSet rs=stmt.getResultSet();
           
        while(rs.next()){
              
                bdid=rs.getInt("id");
                bdcorreo = rs.getString("correo");
                bdcontraseña = rs.getString("contraseña");
                bdnombre= rs.getString("nombre");
                bdsemestre= rs.getInt("semestre");
                bdprograma= rs.getString("programa");
                bdtipo =rs.getInt("tipo");
                
                
                if (bdtipo==2) {
            
                Usuario usu= new Usuario();
                usu.setId(bdid);
                usu.setCorreo(bdcorreo);
                usu.setContrasena(bdcontraseña);
                usu.setNombre(bdnombre);
                usu.setPrograma(bdprograma);
                usu.setSemestre(bdsemestre);
                usu.setTipo(bdtipo);
                u.add(usu);
            }    
        }
        return u;    
    }  
     
     public  ArrayList<Sesion> listarTutorias(String ss) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
             
            ArrayList<Sesion> u= new ArrayList<Sesion>();
            String query1,query2; 
            Usuario us= new Usuario();

             int bdidSesion, bdidTutor, bdidEstudiante, bdPrecio, bdCantidad;
             String bdHoras,bdTema, bdFecha;
             
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection con=DriverManager.getConnection(Util.Util.URL_DB, Util.Util.USER, Util.Util.PW);
            Statement stmt=(Statement) con.createStatement();
            query1="SELECT * FROM TUTORIASESION;";
            stmt.executeQuery(query1);
            ResultSet rs=stmt.getResultSet();
           
        while(rs.next()){
            
              DaoSesion dd= new DaoSesion();
              Usuario g =dd.ObjetoUsuario(ss);
            //  Usuario h=dd.ObjetoTutor(67091);
              
            if(g.getId()==(rs.getInt("idEstudiante"))) {
               
                System.out.println("ENTREEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE");
                
              
                bdPrecio=rs.getInt("precio");
                bdHoras=rs.getString("horas");
                bdTema=rs.getString("tema");
                                  
             Sesion ses = new Sesion();

                ses.setEstudiante(us);
                ses.setTutor(dd.ObjetoTutor(rs.getInt("idTutor")));
                
                ses.setHora(bdHoras);
                ses.setPrecio(bdPrecio);
                
                ses.setTema(bdTema);
                u.add(ses);
                
                  }        
            
                      }
         return u;
        
    }  
     public  ArrayList<Tutoria> OfertaTutorias() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
             
            ArrayList<Tutoria> u= new ArrayList<Tutoria>();
            String query1; 
            
             int bdid, bdidTutor;
             String bdmateria;
             long bdprecio;
             Date bddia;
             String bdhora;
            
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection con=DriverManager.getConnection(Util.Util.URL_DB, Util.Util.USER, Util.Util.PW);
            Statement stmt=(Statement) con.createStatement();
            query1="SELECT * FROM TUTORIATUTOR;";
            stmt.executeQuery(query1);
            ResultSet rs=stmt.getResultSet();
           
        while(rs.next()){
            
            
            DaoSesion dd = new DaoSesion();
            Usuario g = new Usuario();
            
            
            bdidTutor= rs.getInt("idTutor");
            g = dd.ObjetoTutor(bdidTutor);

            bdid = rs.getInt("id");
            bdmateria = rs.getString("materia");
            bdprecio = rs.getLong("precio");
            bddia = rs.getDate("dia");
            bdhora = rs.getString("hora");

            
            Tutoria tut = new Tutoria();

            tut.setIdTutoria(bdid);
            tut.setDia(bddia);
            tut.setHora(bdhora);
            tut.setMateria(bdmateria);
            tut.setPrecio(bdprecio);
            tut.setTutor(g);
            u.add(tut);

            }
         return u;
        
    }  
     public  ArrayList<Sesion> listarSesionesSolicitadas(String ss) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
             
            ArrayList<Sesion> u= new ArrayList<Sesion>();
            String query1,query2; 
            Usuario us= new Usuario();

             int bdidSesion, bdidTutor, bdidEstudiante, bdPrecio, bdCantidad;
             String bdHoras,bdTema, bdFecha;
             
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection con=DriverManager.getConnection(Util.Util.URL_DB, Util.Util.USER, Util.Util.PW);
            Statement stmt=(Statement) con.createStatement();
            query1="SELECT * FROM TUTORIASESION;";
            stmt.executeQuery(query1);
            ResultSet rs=stmt.getResultSet();
           
        while(rs.next()){
            
              DaoSesion dd= new DaoSesion();
              Usuario g =dd.ObjetoUsuario(ss);
             
              
            if(g.getId()==(rs.getInt("idTutor"))) {

                bdPrecio=rs.getInt("precio");
                
                bdHoras=rs.getString("horas");
                bdTema=rs.getString("tema");
                
                 
                
             Sesion ses = new Sesion();

                ses.setEstudiante(dd.ObjetoTutor(rs.getInt("idTutor")));
                ses.setTutor(g);
                
                ses.setHora(bdHoras);
                ses.setPrecio(bdPrecio);
                
                ses.setTema(bdTema);
                u.add(ses);
                
                  }        
            
                      }
         return u;
        
    }  
     
      public  ArrayList<Tutoria> TutoriasCreadas(String ss) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
             
            ArrayList<Tutoria> u= new ArrayList<Tutoria>();
            String query1; 
            
             int bdid, bdidTutor;
             String bdmateria;
             long bdprecio;
             Date bddia;
             String bdhora;
            
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection con=DriverManager.getConnection(Util.Util.URL_DB, Util.Util.USER, Util.Util.PW);
            Statement stmt=(Statement) con.createStatement();
            query1="SELECT * FROM TUTORIATUTOR;";
            stmt.executeQuery(query1);
            ResultSet rs=stmt.getResultSet();
           
        while(rs.next()){
            
            DaoSesion dd = new DaoSesion();
            Usuario t=dd.ObjetoUsuario(ss);
           
            
            if (t.getId()==rs.getInt("idTutor")) {
                
            bdid = rs.getInt("id");
            bdmateria = rs.getString("materia");
            bdprecio = rs.getLong("precio");
            bddia = rs.getDate("dia");
            bdhora = rs.getString("hora");
   
            
            Tutoria tut = new Tutoria();

            tut.setIdTutoria(bdid);
            tut.setDia(bddia);
            tut.setHora(bdhora);
            tut.setMateria(bdmateria);
            tut.setPrecio(bdprecio);
            tut.setTutor(t);
            u.add(tut);
   }
            }
         return u;    
}
     
}            
                

