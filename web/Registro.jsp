<!DOCTYPE HTML>

<html>
    
	<head>
           
		<title>Registro</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
                <script src="js/Constantes.js" type="text/javascript"></script>
                <script src="js/controllerRegistro.js" type="text/javascript"></script>
		<script src="js/jquery.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-xlarge.css" />
		</noscript>
	</head>
	<body id="top">

		<!-- Header -->
			<header id="header" class="skel-layers-fixed">
				<h1><a href="#">UT</a></h1>
				<nav id="nav">
					<ul>
						<li><a href="index.html">Home</a></li>
					</ul>
				</nav>
			</header>

                
                <!-- One -->
			<section id="two" class="wrapper style2">
				<header class="major">
					<h2>¡Bienvenido!</h2>
					<p>Completa los datos para registrarte</p>
				</header>
				<div class="container">
					<div class="row">
						<div class="6u">
							<section class="special">
								<a href="#" class="image fit"><img src="images/LOGGG.png" alt="" /></a>
								<p>Disfruta de una red de estudiantes para estudiantes. Comparte tu conocimiento o solicita una tutoria ahora mismo. Registrate ahora y conéctate con estudiantes como tu!</p>
								<ul class="actions">
									<li><a href="#" class="button alt">Learn More</a></li>
								</ul>
							</section>
						</div>
						<div class="6u">
							<section class="special">
                                                            
                                                             <div class="box">
                                                                 
                                                                 <div class="correousa">
                                                                    <label1>Correo: </label1> 
                                                                    <input type="email" id="correo"class="form-control">
                                                                    <label1>@correo.usa.edu.co </label1>                                                
                                                                 </div>
                                                                 
                                                                    <label>Contraseña: </label>
                                                                    <input type="password" id="contrasena"class="form-control">                                                 
                                                                
                                                                    <label>Confirmacion Contraseña: </label>
                                                                    <input type="password" id="contrasena2" class="form-control">  
                                                                    
                                                                    <label>Nombre: </label>
                                                                    <input type="text" id="nombre"class="form-control">                                                 
                                                                
                                                                
                                                                    <label>Programa: </label>
                                                                    <input type="text" id="programa"class="form-control">                                               
                                                                
                                                                
                                                                    <label>Smestre: </label>
                                                                    <input type="number" id="semestre"class="form-control">                                             
                                                                
                                                               
                                                                    <label>Tipo: </label>
                                                                    <select id="tipo"class="form-control">
                                                                        <option value="1" selected>Estudiante</option>
                                                                        <option value="2" selected=>Tutor</option>
                                                                    </select>
                                                                
                                                                    <button onclick="registro();" class="button big special1">
                                                                            Registrar
                                                                    </button>
                                                                
                                                            
                                                            </div>
							</section>
						</div>
					</div>
				</div>
			</section>

		<!-- Three -->
			
		<!-- Footer -->
			<footer id="footer">
				<div class="container">
					<div class="row double">
						<div class="6u">
							<div class="row collapse-at-2">
								<div class="6u">
									<h3>Integrantes</h3>
									<ul class="alt">
										<li><a href="#">Maria Paula Gomez</a></li>
										<li><a href="#">Luisa Fernanda Ariza</a></li>
										<li><a href="#">Juan Camilo Hernandez</a></li>
										<li><a href="#">Laura Daniela Ramirez</a></li>
                                                                                <li><a href="#">Nicolas Gomez Olaya</a></li>
                                                                        </ul>
								</div>
								<div class="6u">
									<h3>Universidad Sergio Arboleda</h3>
									<ul class="alt">
										<li><a href="#">Calle 74 # 14-14</a></li>
										<li><a href="#">Informacion: (571) 325 8181</a></li>
										<li><a href="#">Linea Gratuita: 01-8000 110414</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="6u">
							<h2>Proyecto UTeach</h2>
							<p>Bienvenido a la mejor aplicacion para programar tus tutorias, hecha por ingenieros de sistemas de la Universidad Sergio Arboleda.</p>
                                                        <img src="images/logo.png">
                                                        <ul class="icons">
								<li><a href="https://twitter.com/usergioarboleda" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
								<li><a href="https://www.facebook.com/usergioarboleda" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
								<li><a href="https://www.instagram.com/usergioarboleda/" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
								<li><a href="https://www.linkedin.com/edu/school?id=11620" class="icon fa-linkedin"><span class="label">LinkedIn</span></a></li>
                                                        </ul>
						</div>
					</div>
					<ul class="copyright">
						<li>&copy; Untitled. All rights reserved.</li>
						<li>Design: <a href="http://templated.co">TEMPLATED</a></li>
						<li>Images: <a href="http://unsplash.com">Unsplash</a></li>
					</ul>
				</div>
			</footer>

	</body>
</html>