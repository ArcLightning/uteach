CREATE DATABASE uteach;
use uteach;
#drop database uteach;
create table Usuario(
id INTEGER (10) primary key,
correo VARCHAR (50),
contraseña VARCHAR (30),
nombre VARCHAR (50),
programa VARCHAR (25),
semestre INTEGER (2),
tipo INTEGER (20)
#estado DOUBLE 
);
INSERT INTO USUARIO VALUES (03075,'nicolas.gomezo@correo.usa.edu.co',123, 'Nicolas Gomez', 'Ing. Sistemas', 7 , 1);
INSERT INTO USUARIO VALUES (67091,'luisaf.ariza',12345, 'Luisa Ariza', 'Ing. de sistemas', 6 , 2);
INSERT INTO USUARIO VALUES (75974,'mariapa.gomez',09876, 'Maria Paula', 'Ing. de sistemas', 5 , 1);

create table TutoriaTutor(
id INTEGER(10),
idTutor INTEGER (10),
materia VARCHAR (30),
precio LONG,
dia VARCHAR(30),
hora VARCHAR(30),
FOREIGN KEY (idTutor) REFERENCES Usuario(id)
);
INSERT INTO TUTORIATUTOR VALUES (0,67091, 'Calculo Diferencial', 15000, 'lunes' , '9:00 - 11:00');

create table TutoriaSesion(
idSesion INTEGER (10) primary key,
idTutor INTEGER (10),
idEstudiante INTEGER(10),
horas varchar (20),
tema VARCHAR (20),
fecha VARCHAR (20),
precio INTEGER(20),
cantidad INTEGER(20),
#calificaion DOUBLE,
FOREIGN KEY (idEstudiante) REFERENCES Usuario(id),
FOREIGN KEY (idTutor) REFERENCES Usuario(id)
);
INSERT INTO TUTORIASESION VALUES (0,67091,75974,'9:00 - 11:00','integrales definidas','12/04/2017',15000,4);

select * from usuario;
select * from 	TutoriaSesion;
select * from 	TutoriaTutor;