package DAO;

import Modelo.Sesion;
import Modelo.Tutoria;
import Modelo.Usuario;
import Util.Conexion;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Nico
 */
public class DaoSesion {
    
    private Connection conexion;
    
    public DaoSesion() throws SQLException{
        Conexion dt = Conexion.getConexion();
        this.conexion = dt.getConnection();
    }
    
    public boolean insertarSesion(Sesion sesion){
        boolean resultado = false;
        try {//!.Establecer la consulta
        String consulta="INSERT INTO TUTORIASESION VALUES(?,?,?,?,?,?,?,?,?)";
        //2.Crear el prepareStament
          PreparedStatement statement;
          statement=this.conexion.prepareStatement(consulta);
        //-------------------------------------------------
            statement.setInt(1,sesion.getIdSesion());
            statement.setInt(2, sesion.getTutor().getId());
            statement.setInt(3, sesion.getEstudiante().getId());
            statement.setString(4, sesion.getHora());
            statement.setString(5, sesion.getTema());
            statement.setString(6, sesion.getFecha());
            statement.setInt(7, sesion.getPrecio());
            statement.setInt(8, sesion.getCalificacion());
            statement.setBoolean(9, true);
          
            System.out.println("watfughjñklkmjnhdxgh.ijlk hgcfx");
        //3.Hacer la ejecucion
        resultado=statement.execute();
        
            
  }catch(SQLException ex){
        ex.printStackTrace();
    }
        return resultado;
}   
    public Usuario ObjetoUsuario(String s) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException{
        
            String query;
            Usuario u= new Usuario();
            
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection con=DriverManager.getConnection(Util.Util.URL_DB, Util.Util.USER, Util.Util.PW);
            Statement stmt=(Statement) con.createStatement();
            query="SELECT * FROM Usuario;";
            stmt.executeQuery(query);
            ResultSet rs=stmt.getResultSet();

        while (rs.next()) {
            if (s.equals(rs.getString("correo"))) {
                
                u.setId(rs.getInt("id"));
                u.setCorreo(rs.getString("correo"));
                u.setNombre(rs.getString("nombre"));
                u.setPrograma(rs.getString("programa"));
                u.setSemestre(rs.getInt("semestre"));
                break;
            
            }
        }
        System.out.println(u.toString());
        return u;
       
    }
    
    public Usuario ObjetoTutor(int id) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException{
        
            String query;
            Usuario u= new Usuario();
            
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection con=DriverManager.getConnection(Util.Util.URL_DB, Util.Util.USER, Util.Util.PW);
            Statement stmt=(Statement) con.createStatement();
            query="SELECT * FROM Usuario;";
            stmt.executeQuery(query);
            ResultSet rs=stmt.getResultSet();

        while (rs.next()) {
             
            if (id== rs.getInt("id")) {
                u.setId(rs.getInt("id"));
                u.setCorreo(rs.getString("correo"));
                u.setNombre(rs.getString("nombre"));
                u.setPrograma(rs.getString("programa"));
                u.setSemestre(rs.getInt("semestre"));
              }  
        }
        return u;
       
    }
    
    public ArrayList<Tutoria> FiltroMateria(String materia) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException{
        
            String query;
            Tutoria t= new Tutoria();
            ArrayList<Tutoria> u= new ArrayList<Tutoria>();
            String bdmateria,bddia,bdhora;
            int bdidTutor;
            
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection con=DriverManager.getConnection(Util.Util.URL_DB, Util.Util.USER, Util.Util.PW);
            Statement stmt=(Statement) con.createStatement();
            query="SELECT * FROM tutoriatutor;";
            stmt.executeQuery(query);
            ResultSet rs=stmt.getResultSet();

        while (rs.next()) {
            
            DaoSesion dao= new DaoSesion();
            
            if (materia.equalsIgnoreCase(rs.getString("materia"))) {
                
                t.setTutor(dao.ObjetoTutor(rs.getInt("idTutor")));
                t.setDia(rs.getDate("dia"));
                t.setHora(rs.getString("hora"));
                t.setMateria(rs.getString("materia"));
                t.setPrecio(rs.getInt("precio"));
                
                 u.add(t);
            }
        }
           
        return u;
}
}
